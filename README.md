# Python Donodoo

Donodoo API Client

### Requisiti

E' necessario avere installati i seguenti pacchetti

- python (2.7, 3.4, 3.5)
- requests (2.18.1)

### Installazione

Per installare la libreria e' sufficiente digitare:

        pip install python-donodoo

